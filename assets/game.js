const TEAM_X = 'X';
const TEAM_O = 'O';

let game = {
    currentTeam: TEAM_O,
    id: -1,
    playerID: -1,
    myTurn: false,
    myTeam: -1,
    opponentTeam: -1,
    over: false,
    lastBoardState: null,
    
    getTeamClass: function (team) {
        return (team === TEAM_O) ? 'team-o' : 'team-x';
    },


    postMoveDetails: function (x, y) {
        $.post('put', {x: x, y: y, game_id: this.id}, responseJSON => {
            let response = JSON.parse(responseJSON);
            console.log(response);
            if (response.status !== 'Error') {
                let winner = response.outcome || 'Error';
                if (winner !== 'Draw' && winner !== 'None') {
                    game.over = true;
                    alert('Team ' + winner  + ' wins!');
                }
            }

        });
    },
    
    canPlaceTile: function () {
        return !(this.over) && (this.myTurn);
    },

    updateGameState: function () {
        $.post('board', { game_id: this.id }, responseJSON => {
            if (this.lastBoardState !== responseJSON) {
                let boardData = JSON.parse(responseJSON);
                boardData.map(l => {
                    let el = $('.game-table td[data-x="'+l.x+'"][data-y="'+l.y+'"]');
                    if (l.player_id === this.playerID) {
                        el.addClass(this.getTeamClass(this.myTeam));
                        el.html(this.myTeam);
                    } else {
                        el.addClass(this.getTeamClass(this.opponentTeam));
                        el.html(this.opponentTeam);
                    }
                });
                this.lastBoardState = responseJSON;
                this.myTurn = !this.myTurn;
            }

        }).fail(() => {
            console.error("Error occurred loading board state");
        });
    }

};

$('.game-table td').click(function(ev) {
    const cellClass = this.className;
    if (cellClass !== game.getTeamClass(game.myTeam) && game.canPlaceTile()) {
        $(this).addClass(game.getTeamClass(game.myTeam));
        let moveX = $(this).data('x');
        let moveY = $(this).data('y');
        game.postMoveDetails(moveX, moveY);
        this.myTurn = !this.myTurn;
    } else {
        console.log("Invalid move");
    }
    
    game.updateGameState();
    
});

$(document).ready(function() {
    // RULES:
    // 1. Host is X
    // 2. X moves first
    // 3. Opponent cannot move while waiting for other player to move.
    game.id = $('#game-id-field').val();
    window.history.pushState('', '', '?id=' + game.id);
    game.playerID = $('#player-id-field').val();
    game.myTeam = $('#my-team-field').val();
    game.opponentTeam = (game.myTeam === TEAM_X) ? TEAM_O : TEAM_X;
    game.myTurn = (game.myTeam === TEAM_X);
    
    $('#game-status-text').html('Team ' + game.currentTeam + '\'s turn');
    
    game.updateGameState();
    // TODO TURN ME BACK ON!! (You know you're going to waste so much time on this...)
    setInterval(() => game.updateGameState(), 3000);
});

window.game = game; // Easier debugging