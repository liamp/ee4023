$('#login-button').click(function (ev) {
    ev.preventDefault();
    $.post('/ee4023/trylogin', $('#login-form').serialize(), function (responseJson) {
        const response = JSON.parse(responseJson);
        if (response.status !== 'Error') {
            window.location.href = response.redirect;
        } else {
            console.log("Got here");
            $('.error-message').css('display', 'block');
            $('.error-message').html('Error: ' + response.message);
        }
    });
});

$('#register-button').click(function (ev) {
    ev.preventDefault();
    console.log($('#register-form').serialize());
    $.post('/ee4023/tryregister', $('#register-form').serialize(), function (responseJson) {
        const response = JSON.parse(responseJson);
        if (response.status !== 'Error') {
            window.location.href = response.redirect;
        } else {
            $('.error-message').css('display', 'block');
            $('.error-message').html('Error: ' + response.message);
        }
    })
})


