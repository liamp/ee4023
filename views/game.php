<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/main.css">
    <title>Tic Tac Toe</title>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <h4 class="title">Tic Tac Toe</h4>
    <input id="game-id-field" type="hidden" value="<?php echo $game_id; ?>">
    <input id="player-id-field" type="hidden" value="<?php echo $player_id; ?>">
    <input id="my-team-field" type="hidden" value="<?php echo $my_team; ?>">
    <p id="game-status-text" class="message secondary"></p>
    <p id="your-team-text" class="message secondary">You are team <?php echo $my_team; ?></p>
    <table class="game-table">
        <tr>
            <td data-x="0" data-y="0" data-cell="4">&nbsp;</td>
            <td data-x="1" data-y="0" data-cell="9">&nbsp;</td>
            <td data-x="2" data-y="0" data-cell="2">&nbsp;</td>
        </tr>
        <tr>
            <td data-x="0" data-y="1" data-cell="3">&nbsp;</td>
            <td data-x="1" data-y="1" data-cell="5">&nbsp;</td>
            <td data-x="2" data-y="1" data-cell="7">&nbsp;</td>
        </tr>
        <tr>
            <td data-x="0" data-y="2" data-cell="8">&nbsp;</td>
            <td data-x="1" data-y="2" data-cell="1">&nbsp;</td>
            <td data-x="2" data-y="2" data-cell="6">&nbsp;</td>
        </tr>
    </table>
</div>
<script type="text/javascript" src="assets/game.js"></script>
</body>
</html>

