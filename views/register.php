<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Register</title>

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="title">Register</h4>
            <form class="form" id="register-form">
                <label for="username">Username</label>
                <input name="username" id="username" type="text">
                
                <label for="first_name">First Name</label>
                <input name="first_name" id="first_name" type="text">
                
                <label for="first_name">Last Name</label>
                <input name="last_name" id="last_name" type="text">
                
                <label for="password">Password</label>
                <input name="password" id="password" type="password">
                
                <div class="error-message" style="display: none"></div>
                <button id="register-button" class="form-button" type="submit">Sign Up</button>
            </form>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>

