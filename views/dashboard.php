<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Tic-Tac-Toe</title>

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="title">Welcome to Tic-Tac-Toe</h4>
            <div class="flex-row mtop-1">
                <a href="my-scores" class="form-button">My Games</a>
                <a href="leaderboard" class="form-button">Leaderboard</a>
                <a id="new-game-button" href="game" class="form-button">New Game</a>
            </div>
        </div>
        <div class="container mtop-1">
            <h4 class="title">Open Games</h4>
            <?php if ($has_games !== false): ?>
                <ul>
                    <?php foreach ($open_games as $game): ?>
                        <li>
                            <a href="game?id=<?php echo $game->id; ?>">
                                <?php echo $game->user; ?>'s game
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <p class="message secondary">There are currently no open games</p>
            <?php endif; ?>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>

