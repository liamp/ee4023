<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Register</title>

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="error-msg">Not found</h4>
            <p>The page you were looking for was not found.</p>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>

