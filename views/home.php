<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Tic Tac Toe</title>

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="title">Welcome to Tic-Tac-Toe</h4>
            <div class="flex-row">
                <a href="register" class="form-button">Register</a>
                <a href="login" class="form-button">Log In</a>
            </div>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>

