<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Leaderboard</title>

        <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="title">Leaderboard</h4>
            <?php if ($has_games !== false): ?>
                <ul>
                    <?php foreach ($league_table as $game): ?>
                        <li>
                            <strong><?php echo $game->user0; ?></strong> vs <strong><?php echo $game->user1; ?></strong>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <p class="message secondary">There are currently no open games</p>
            <?php endif; ?>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>

