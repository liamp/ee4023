<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/main.css">
    <title>My Games</title>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <h4 class="title">My Games</h4>
    <?php if ($has_games !== false): ?>
        <ul>
            <?php foreach ($my_scores as $game): ?>
                <li>
                    <strong><?php echo $game->user; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p class="message secondary">You don't have any games at the moment</p>
    <?php endif; ?>
</div>
<script type="text/javascript" src="assets/main.js"></script>
</body>
</html>

