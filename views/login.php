<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="assets/main.css" >
        <title>Login</title>

<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <h4 class="title">Login</h4>
            <form class="form" id="login-form" action="ee4023/trylogin" method="POST">
                <label for="username">Username</label>
                <input name="username" id="username" type="text">
                <label for="password">Password</label>
                <input name="password" id="password" type="password">
                <div class="error-message" style="display: none"></div>
                <button id="login-button" class="form-button" type="submit">Log In</button>
            </form>
        </div>
        <script type="text/javascript" src="assets/main.js"></script>
    </body>
</html>
