<?php

session_start();

const ROOT_DIR = '/ee4023';
const WSDL_URL = 'http://localhost:8080/TTTWebApplication/TTTWebService?WSDL';
const ERROR_BAD_CONNECTION_1 = -1;
const ERROR_BAD_CONNECTION_2 = -2;
const ERROR_INVALID_CREDENTIALS = 0;

const ROUTES = [
    '/' => 'get_home',
    '/login' => 'get_login',
    '/dashboard' => 'get_dashboard_auth',
    '/game' => 'get_game_auth',
    '/trylogin' => 'post_login',
    '/register' => 'get_register',
    '/tryregister' => 'post_register',
    '/my-scores' => 'get_my_scores_auth',
    '/leaderboard' => 'get_leaderboard_auth',
    '/put' => 'post_game_move_auth',
    '/board' => 'get_game_state_auth',
];

function create_soap_client() {
    return new SoapClient(WSDL_URL, ['trace' => true, 'exceptions' => true, 'keep_alive' => false]);
}

function ends_with(string $haystack, string $needle) {
    if ($needle === '') {
        return true;
    }
    $diff = \strlen($haystack) - \strlen($needle);
    return $diff >= 0 && strpos($haystack, $needle, $diff) !== false;
}

function parse_route(string $route) {
    $route_without_query = preg_replace('/\?.*/', '', $route);
    $route_without_base = ltrim($route_without_query, '/');
    $route_without_base = substr($route_without_base, strpos($route_without_base, '/'));
    if (in_array($route_without_base, array_keys(ROUTES))) {
        return $route_without_base;
    } else {
        return false;
    }
}

function pass_or_die(callable $fn, ...$params) {
    $result = call_user_func_array($fn, $params);
    if ($result !== false) {
        return $result;
    } else {
        echo json_encode(['status' => 'Error', 'message' => 'Invalid data: ' . print_r($params, true)]);
        http_response_code(200);
        die();
    }
}

function check_route_auth($route) {
    if (ends_with($route, 'auth')) {
        if (isset($_SESSION['user_id'])) {
            return true;
        } else {
            return false;
        }
    }

    return true;
}

function parse_db_output(string $db_output, array $field_names) {
    $lines = explode("\n", $db_output);
    $parsed_output = array_map(function ($line) use ($field_names) {
        $data = str_getcsv($line);
        $class = new stdClass();
        array_walk($field_names, function ($element, $index) use ($data, $class) {
            $class->{$element} = $data[$index];
            return $class;
        },  array_keys($field_names));

        return $class;
    }, $lines);

    return $parsed_output;
}

function current_user_id() {
    return $_SESSION['user_id'] ?? -1;
}

function redirect(string $new_route) {
    header('Location: ' . $new_route);
    die();
}

function render_template(string $view_name, array $data = []) {
    $view_path = "views/$view_name.php";
    extract($data);
    ob_start();

    if (file_exists($view_path)) {
        include $view_path;
    } else {
        include 'views/not_found.php';
    }

    return ob_get_clean();
}

function post_login() {
    $soap_client = create_soap_client();

    $username = pass_or_die('filter_input', INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = pass_or_die('filter_input', INPUT_POST, 'password', FILTER_SANITIZE_STRING);

    $request = ['username' => $username, 'password' => $password];
    $response = $soap_client->login($request);
    $user_id = (int) $response->return;

    if ($user_id === ERROR_BAD_CONNECTION_1 || $user_id === ERROR_BAD_CONNECTION_2) {
        $response_data = json_encode(['status' => 'Error', 'message' => 'There was a problem connecting to the game web service, try again later.']);
    } else if ($user_id == ERROR_INVALID_CREDENTIALS) {
        $response_data = json_encode(['status' => 'Error', 'message' => 'Invalid credentials']);
    } else {
        $response_data = json_encode(['status' => 'OK', 'redirect' => 'dashboard']);
        $_SESSION['user_id'] = $user_id;
    }

    echo $response_data;
}

function get_game_state_auth() {
    $soap_client = create_soap_client();
    $game_id = pass_or_die('filter_input', INPUT_POST, 'game_id', FILTER_VALIDATE_INT);
    $soap_request = ['gid' => $game_id];
    $soap_response = $soap_client->getBoard($soap_request)->return;

    $board_state = [];
    if ($soap_response !== 'ERROR-NOMOVES') {
        $board_state = parse_db_output($soap_response, ['player_id', 'x', 'y']);
    }


    echo json_encode($board_state);
}

function get_my_scores_auth() {
    $soap_client = create_soap_client();
    $soap_request = ['uid' => current_user_id()];
    $soap_response = $soap_client->showAllMyGames($soap_request)->return;
    $has_games = false;
    $my_scores = [];
    if ($soap_response !== 'ERROR-NOGAMES') {
        $my_scores = parse_db_output($soap_response, ['user']);
    }

    echo render_template('my_scores', ['has_games' => $has_games, 'my_scores' => $my_scores]);
}

function get_login() {
    if (current_user_id() === -1) {
        echo render_template('login');
    } else {
        redirect('dashboard');
    }
}

function get_game_auth() {
    // If GET param ?id is set, attempt to join a game with that id, otherwise
    // create a new game.
    $soap_client = create_soap_client();
    if (isset($_GET['id'])) {
        $game_id = (int) pass_or_die('filter_input', INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $request = ['uid' => current_user_id(), 'gid' => $game_id];
        $response = $soap_client->joinGame($request)->return;
        if ($response === '1') {
            echo render_template('game', ['game_id' => $game_id, 'player_id' => current_user_id(), 'my_team' => 'O']);
        }
    } else {
        // No id found, create a new game
        $request = ['uid' => current_user_id()];
        $game_id = (int) $soap_client->newGame($request)->return;
        echo render_template('game', ['game_id' => $game_id, 'player_id' => current_user_id(), 'my_team' => 'X']);
    }
}

function get_register() {
    if (current_user_id() === -1) {
        echo render_template('register');
    } else {
        redirect('dashboard');
    }
}

function post_game_move_auth() {
    $move_x = pass_or_die('filter_input', INPUT_POST, 'x', FILTER_SANITIZE_STRING);
    $move_y = pass_or_die('filter_input', INPUT_POST, 'y', FILTER_SANITIZE_STRING);
    $game_id = pass_or_die('filter_input', INPUT_POST, 'game_id', FILTER_SANITIZE_STRING);

    $soap_client = create_soap_client();
    $soap_request = ['x' => $move_x, 'y' => $move_y, 'gid' => $game_id];
    $soap_response = (int) $soap_client->checkSquare($soap_request)->return;
    if ($soap_response === 0) {
        $soap_request = ['x' => $move_x, 'y' => $move_y, 'gid' => $game_id, 'pid' => current_user_id()];
        $soap_response = (int) $soap_client->takeSquare($soap_request)->return;

        if ($soap_response !== 0) {

            $soap_request = ['gid' => $game_id];
            $winner = $soap_client->checkWin($soap_request)->return;
            $outcomes = ['None', 'X', 'O', 'Draw'];
            $outcome = $outcomes[$winner] ?? 'Error';

            echo json_encode(['status' => 'OK', 'outcome' => $outcome]);
        } else {
            echo json_encode(['status' => 'Error']);
        }
    } else {
        echo json_encode(['status' => 'Error']);
    }
}

function get_home() {
    if (current_user_id() === -1) {
        echo render_template('home');
    } else {
        redirect('dashboard');
    }
}

function get_dashboard_auth() {
    $soap_client = create_soap_client();
    $soap_response = $soap_client->showOpenGames()->return;
    $has_games = true;
    $open_games = [];
    if ($soap_response === 'ERROR-NOGAMES') {
        $has_games = false;
    } else {
        $open_games = parse_db_output($soap_response, ['id', 'user']);
    }

    echo render_template('dashboard', ['has_games' => $has_games, 'open_games' => $open_games]);
}

function get_leaderboard_auth() {
    $soap_client = create_soap_client();
    $soap_response = $soap_client->leagueTable()->return;
    $league_table = [];
    $has_games = false;
    if ($soap_response !== 'ERROR-NOGAMES') {
        $has_games = true;

        $league_table = parse_db_output($soap_response, ['id', 'user0', 'user1', 'winner']);
    }

    echo render_template('leaderboard', ['has_games' => $has_games, 'league_table' => $league_table]);
}

function post_register() {
    $soap_client = create_soap_client();

    $username = pass_or_die('filter_input', INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = pass_or_die('filter_input', INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $first_name = pass_or_die('filter_input', INPUT_POST, 'first_name', FILTER_SANITIZE_STRING);
    $last_name = pass_or_die('filter_input', INPUT_POST, 'last_name', FILTER_SANITIZE_STRING);

    $request = ['username' => $username, 'password' => $password, 'name' => $first_name, 'surname' => $last_name];
    $response = $soap_client->register($request);

    $errors = [
        'ERROR-REPEAT' => 'That username is already registered',
        'ERROR-INSERT' => 'Application error occurred, please try again later',
        'ERROR-RETRIEVE' => 'Application error occurred, please try again later',
        'ERROR-DB' => 'Application error occurred, please try again later',
    ];

    if (isset($errors[$response->return])) {
        $response_data = json_encode(['status' => 'Error', 'message' => $errors[$response->return]]);
    } else {
        $user_id = (int) $response->return;
        $_SESSION['user_id'] = $user_id;
        $response_data = json_encode(['status' => 'OK', 'redirect' => 'dashboard']);
    }

    echo $response_data;
}

function main() {
    $route = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_CALLBACK, ['options' => 'parse_route']);
    if ($route) {
        $route_func = ROUTES[$route];
        if (check_route_auth($route_func)) {
            call_user_func(ROUTES[$route]);
        } else {
            echo render_template('unauthorized');
        }
    } else {
        echo render_template('not_found');
        http_response_code(404);
        die();
    }
}

// Run application
main();
